package com.kshrd.roombooking.ui.home;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.kshrd.roombooking.R;

public class MyHolder extends RecyclerView.ViewHolder {

    public TextView hallName;
    public TextView Capacity;
    public ImageView imageView;
    public TextView roomName;
    public TextView roomType;

    public MyHolder(@NonNull View itemView) {
        super(itemView);
        Capacity = itemView.findViewById(R.id.txtCapacity);
        hallName = itemView.findViewById(R.id.txtHallName);
        roomName = itemView.findViewById(R.id.room_name);
        roomType = itemView.findViewById(R.id.room_title);
        imageView = itemView.findViewById(R.id.imageViewHall);
    }

}
