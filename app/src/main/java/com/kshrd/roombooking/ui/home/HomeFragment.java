package com.kshrd.roombooking.ui.home;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.material.navigation.NavigationView;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.kshrd.roombooking.R;
import com.kshrd.roombooking.model.HallRoom;
import com.squareup.picasso.Picasso;

import java.util.List;

public class HomeFragment extends Fragment {

    FirebaseRecyclerAdapter<HallRoom,MyHolder> adapter;
    FirebaseRecyclerOptions<HallRoom> hallRooms;
    DatabaseReference reference;
    private Toolbar toolbar;
    private List<HallRoom> hallRoomList;
    private Button btn;
    private RecyclerView recyclerView;
    private DrawerLayout drawerLayout;
    private NavigationView navigationView;
    private FirebaseUser currentUser;
    static final String TAG = "HomeScreen";
    Context context;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.fragment_home, container, false);
        recyclerView = root.findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);

        LinearLayoutManager layoutManager = new LinearLayoutManager(context);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        loadData();

        return root;
    }

    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    private void loadData() {
        reference = FirebaseDatabase.getInstance().getReference();
        hallRooms = new FirebaseRecyclerOptions.Builder<HallRoom>().setQuery(reference.child("/Room"), HallRoom.class).build();

        adapter = new FirebaseRecyclerAdapter<HallRoom, MyHolder>(hallRooms) {

            @Override
            protected void onBindViewHolder(@NonNull MyHolder holder, int position, @NonNull HallRoom model) {
                holder.Capacity.setText(String.valueOf(model.getCapacity()));
                holder.hallName.setText(model.getHallName());
                holder.roomName.setText(model.getRoomName());
                holder.roomType.setText(model.getRoomType());

                Picasso.get().load(model.getImage()).into(holder.imageView);
//                holder.imageView.setOnClickListener(v -> {
//                    Bundle bundle = new Bundle();
//                    bundle.putString("roomKey", getRef(position).getKey());
//                    Fragment fragment = new booking();
//                    fragment.setArguments(bundle);
//                    FragmentTransaction ts = getSupportFragmentManager().beginTransaction();
//                    ts.replace(R.id.drawer, fragment);
//                    ts.addToBackStack(null);
//                    ts.commit();
//                });
            }

            @NonNull
            @Override
            public MyHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
                LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
                View listItem = layoutInflater.inflate(R.layout.my_list_custom, parent, false);
                return new MyHolder(listItem);
            }
        };

        recyclerView.setAdapter(adapter);
    }
}