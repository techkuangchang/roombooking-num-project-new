package com.kshrd.roombooking.model;

import com.google.firebase.database.IgnoreExtraProperties;

@IgnoreExtraProperties
public class HallRoom {

    private int capacity;
    private String hallName;
    private String roomName;
    private String roomType;
    private String image;

    public HallRoom(int capacity, String hallName, String roomName, String roomType, String image) {
        this.capacity = capacity;
        this.hallName = hallName;
        this.roomName = roomName;
        this.roomType = roomType;
        this.image = image;
    }


    public HallRoom() {
    }

    public int getCapacity() {
        return capacity;
    }

    public void setCapacity(int capacity) {
        this.capacity = capacity;
    }

    public String getHallName() {
        return hallName;
    }

    public void setHallName(String hallName) {
        this.hallName = hallName;
    }

    public String getRoomName() {
        return roomName;
    }

    public void setRoomName(String roomName) {
        this.roomName = roomName;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

}
